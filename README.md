# Reproduction Code for DataDog Tracer Go library

## Prerequisites

### Setup a local postgres database

```shell
docker run -p 5432:5432 --name notes -e POSTGRES_PASSWORD=postgres -d postgres
psql -U postgres -h localhost -p 5432 -c "CREATE DATABASE notes"
cat <<EOF | psql -U postgres -h localhost -p 5432 -d notes
CREATE TABLE notes
(
  id          uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  title       VARCHAR NOT NULL,
  description VARCHAR NOT NULL,
  updated_at  TIMESTAMP NOT NULL DEFAULT NOW()
);
EOF
```

### Compile

```shell
go build
```

### Run

```shell
./tracerbug
```

## Demonstration

### Success scenario

Run these commands (yep, twice)

```shell
curl -X GET 'http://localhost:8888/works'
curl -X GET 'http://localhost:8888/works'
```

And the output should show in both case

```text
{"error": "no notes found"}%
```

### Failure scenario

Run these commands (yep, twice)

```shell
curl -X GET 'http://localhost:8888/fails'
curl -X GET 'http://localhost:8888/fails'
```

And the output should show this in the 2nd case:

```text
{"error": "timeout: context already done: context canceled'}
``````
