package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/caarlos0/env"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/stdlib"
	sqltrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/database/sql"
	gormtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorm.io/gorm.v1"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type contextKey struct{}

type Note struct {
	Id          uuid.UUID `gorm:"primaryKey;type:uuid;default:uuid_generate_v4()" json:"id"`
	Title       string
	Description string
	UpdatedAt   time.Time `gorm:"column:updated_at"`
}

type Configuration struct {
	PgUri      string `env:"PG_URI"       envDefault:"postgres://postgres:postgres@localhost:5432/notes?sslmode=disable"`
	ListenAddr string `env:"LISTEN_ADDR"  envDefault:":8888"`
}

type RequestHandler struct {
	Db *gorm.DB
}

var (
	noTraceDb *gorm.DB
	tracedDb  *gorm.DB
)

func main() {
	var config Configuration
	if err := env.Parse(&config); err != nil {
		log.Fatal(err)
	}

	router := chi.NewRouter()

	tracedDb, err := NewGORM(config.PgUri)
	if err != nil {
		log.Fatal(err)
	}
	err = Migrate(tracedDb, &Note{})
	if err != nil {
		log.Fatal(err)
	}
	tracedHandler := RequestHandler{Db: tracedDb}

	noTraceDb, err := NewGORMNoTrace(config.PgUri)
	if err != nil {
		log.Fatal(err)
	}
	err = Migrate(noTraceDb, &Note{})
	if err != nil {
		log.Fatal(err)
	}
	noTraceHandler := RequestHandler{Db: noTraceDb}

	router.Get("/fails", tracedHandler.TraceHandler)
	router.Get("/works", noTraceHandler.TraceHandler)

	http.ListenAndServe(config.ListenAddr, router)
}

func readDB(ctx context.Context, db *gorm.DB) {
	var notes []Note
	res := db.WithContext(ctx).Find(&notes)
	switch {
	case res.Error != nil:
		log.Fatal(res.Error)
	case res.RowsAffected == 0:
		fmt.Println("expected no results found")
	default:
		log.Fatal("unexpected results")
	}
}

func (h RequestHandler) TraceHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var notes []Note
	res := h.Db.WithContext(r.Context()).Find(&notes)
	switch {
	case res.Error != nil:
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf(`{"error": "%s'}`, res.Error.Error())))
	case res.RowsAffected == 0:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"error": "no notes found"}`))
	default:
		response, err := json.Marshal(notes)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf(`{"error": "%s'}`, res.Error.Error())))
		} else {
			w.WriteHeader(http.StatusOK)
			w.Write(response)
		}
	}
}

func NewGORMNoTrace(pgURI string) (*gorm.DB, error) {
	db, err := gorm.Open(postgres.New(postgres.Config{DSN: pgURI}), &gorm.Config{})
	if err != nil || db == nil {
		return nil, fmt.Errorf("failed to connect to postgres: %w", err)
	}
	return db, nil
}

func NewGORM(pgURI string) (*gorm.DB, error) {
	sqltrace.Register("pgx", &stdlib.Driver{}, sqltrace.WithServiceName("tracerbug"))

	sqlDb, err := sqltrace.Open("pgx", pgURI)
	if err != nil {
		return nil, err
	}

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,   // Slow SQL threshold
			LogLevel:                  logger.Silent, // Log level
			IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
			ParameterizedQueries:      true,          // Don't include params in the SQL log
			Colorful:                  false,         // Disable color
		},
	)

	db, err := gormtrace.Open(postgres.New(postgres.Config{Conn: sqlDb}), &gorm.Config{
		PrepareStmt: true,
		Logger:      newLogger,
	})

	if err != nil || db == nil {
		return nil, fmt.Errorf("failed to connect to postgres: %w", err)
	}
	return db, nil
}

func Migrate(gormClient *gorm.DB, models ...interface{}) error {
	if err := gormClient.Migrator().AutoMigrate(models...); err != nil {
		return err
	}
	return nil
}
